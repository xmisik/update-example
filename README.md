# Electron-Updater with GitLab

This project demonstrates using the electron-updater autoUpdater with a Gitlab host and the following setup:
- GitLab hosts the Project
- The Project is a **Private Project**
- When there is a commit to the default respository, the GitLab CI performs the following actions:
    1. Builds the Project
    2. Uploads the Project *.exe file as a Generic Package
    3. Releases the project

## Setup
1. Setup your own repository with these files or clone the repository.
2. Create a Private Token for use by the program. The Token will need at least `read_api` access. 
> NOTE: Different tokens allow different degrees of access. A [Project Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-access-tokens) is preferred, however the free GitLab.com platform does not allow Project Tokens so a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) should be created. 
It is suggested that you use a `Project Token` if you are using any GitLab version other than the free GitLab.com version. The selfhosted GitLab does allow the use of `Project Tokens`.
3. Copy the Private Token and insert it in `main.js` here:
    ```javascript
    ///////////////////
    // Auto upadater //
    ///////////////////
    autoUpdater.requestHeaders = { 'PRIVATE-TOKEN': 'your-private-token-here' }
    autoUpdater.autoDownload = true
    autoUpdater.logger = log
    autoUpdater.logger.transports.file.level = 'info'
    ```
    #### !!!It is not secure to commit your Private-Token. If you accidentally do commit it, delete that token and create a new one!!!

Other changes to `main.js` may include adjusting the frequency that the project checks for updates. I have set the project to check 10sec after the program is started and every 2 minutes after that. This is likely way too often for real-life use. I also have copius logging. In order to see the logging from the installed program, launch the program from `PowerShell` or `CMD`.

4. From the homepage of your repository, copy the `Project Id`

![image.png](./image.png)

5. Update your `project.json` file to reflect your repository information (including your repository URL and your project ID), here:
    ```javascript
    {
    "name": "electron-updater-gitlab",
    "productName": "MyApp",
    "version": "1.0.0",
    ...
    "repository": {
        "type": "git",
        "url": "git+https://gitlab.com/youruser/your-repository.git",
        "release": "latest"
    },
    "build": {
        "appId": "com.dpieski.ElectronUpdaterApp",
        "artifactName": "${productName}-${version}.${ext}",
        "publish": {
        "provider": "generic",
        "url": "https://gitlab.com/api/v4/projects/[your-project-id]/jobs/artifacts/master/raw/dist?job=build"
        },
    ...
    }
    ```
    > NOTE: the `artifactName` element will be the name of your exe file when your project is built. The `artifactName` includes the `productName` and `version` elements. Both of these elements are also used in the `.gitlab-ci.yml` file and are thus needed for successful execution.

The URL here should reference the [artifact folder location](https://docs.gitlab.com/ee/api/job_artifacts.html#download-a-single-artifact-file-from-specific-tag-or-branch) referenced below in the `Build Stage`. 

6. Commit your project to GitLab to the `default` branch.

## Review of .gitlab-ci.yml
The gitlab-ci includes three stages: Build, Upload, and Release.
### Build Stage
The Build stage first, installes the dependencies of your project. It then uses node to extract the `version` and the `productName` values from your `package.json` file. These values are then used throughout the rest of the Pipelines.

The Build stage provides the variables.env file as an [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#job-artifacts), as well as providing access to the `/dist/*.*` path where the project files were build to, including the *.exe file, the *.exe.blockmap file, and the latest.yml file (which is needed as part of the auto-update process). This format will exclude the `/win-unpacked/` folder from being added as an artifact.

### variables
Two variables are then set: WIN_BINARY with is set to the name of the *.exe file. The format of the WIN_BINARY variable should match the `artifactName` format used in the `package.json` file. 
The second variable set is PACKAGE_REGISTRY_URL. The `$APPNAME` portion of the directory corresponds with the Package Name in the Package Registry:
![image-1.png](./image-1.png)

### Upload Stage
The Upload stage uploads the *.exe artifact, generated in the Build stage, to the Package Registry.
This step uses the [Package Registry publish api](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file).

### Release Stage
The Release stage uses the [release-cli](https://gitlab.com/gitlab-org/release-cli/-/blob/master/docs/index.md). The release consists of a name "Release [version], a tag-name for the release set to 'v[version]', a description, and an assets-link with a name of the AppName and a URL linked to the Package Registry. 

By Uploading the *.exe to the registry, the exe is still accessible when your tags automatically expire (gitlab default is to keep the last 10 tags and expire other tags >90 days old). This would allow users to access older versions of the exe file if needed. 

If keeping the exe in the Package Registry is not desireable, you can remove the Upload Stage from the `.gitlab-ci.yml` file and remove the `--assets-link` portion of the Release Stage. 

---

# Considerations and Room for Improvement
1. Find a better way to use the Private Token to access GitLab. 
2. Don't execute when other files (like the README file) are updated and committed. 
3. Update the `.gitlab-ci.yml` file to use a `yml` format for the Release instead of the script based on a response from gitlab-org/gitlab#270582 since [GitLab doesn't yet implement `assets-link` in the `yml` configuration](https://docs.gitlab.com/ee/ci/yaml/README.html#release)

---

# References
The following projects and documents were the primary references for this project:
1. [Release Command Line Tool](https://gitlab.com/gitlab-org/release-cli/-/blob/master/docs/index.md#using-this-tool-in-gitlab-ci)
1. [Package Registry URL](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file)
1. [Releasing Assets as Generic Packages](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs/examples/release-assets-as-generic-package/)
1. How to access `package.json` from the CI. [StackOverflow by dx_over_dt](https://stackoverflow.com/a/61432476/1622899)
1. [Electron Updater gist from Slauta](https://gist.github.com/Slauta/5b2bcf9fa1f6f6a9443aa6b447bcae05) and [their StackOverflow comment](https://stackoverflow.com/a/46559282/1622899)
    - [Updated based on carlosc-dev's comment](https://gist.github.com/Slauta/5b2bcf9fa1f6f6a9443aa6b447bcae05#gistcomment-3002794)



